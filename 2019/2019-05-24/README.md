# Simple Docker Example

This is a very simple example of [Docker](https://www.docker.com/) running a simple NodeJS app.

Things to know. Containers are not VMs, they are more like a root jail. They use the host OS kernel and processes running in the container are visible to the host OS. Docker is just one of many ways to use containers. [Proxmox](https://www.proxmox.com) is another, and it can also run VMs.

This example requires a Linux system with Docker installed. 
[Installing Docker for Debian](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-debian-9)
[Installing Docker for CentOS](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-centos-7)

For more information see [this cheat sheet](https://github.com/wsargent/docker-cheat-sheet).

Almost all of these steps require permission to execute privileged commands. In a production environment, a special group would be created with permissions to run these commands. If you are new to Linux the __sudo__ command is commonly used to run commands as the root administrator, just prefix the commands with __sudo__. It is recommended that you practice this on a test system.

# Image Files
Image files are a collection of Linux OS files needed to run in a container. They contain a directory tree like /bin/, /lib/, /usr/, /var/ and others, everything needed to run the required programs.

One popular image is BusyBox were hundreds of Linux programs such as ls, grep, find, cat are all just one binary program. This makes the image very small.

This sample project uses alpine-node, a small image with a functional NodeJS.

# Building A Simple Project
Three files are included with this project.

## service.js
This is a simple JavaScript to be run by NodeJS


## package.json
This file tells NodeJS about the code to run.

It tells NodeJS:
 - the name of the script to start
 - dependencies that are needed
 - how to start the script

## Dockerfile
This tells Docker how to build an image that contains your project.

It tells Docker
 - what image to source the build from, this one uses __mhart/alpine-node__
 - where to install code files

The source image __mhart/alpine-node__ is a very small Linux that includes a NodeJS service.


# Building Images
On a Linux system where you have a running Docker, change to the working code directory where the __Dockerfile__ is, then build a new image.
```
docker build -t <new image title> .
```

This will copy the __mhart/alpine-node__ image and add the project code to it as instructed by the contents of the __Dockerfile__.

If your docker does not have the __mhart/alpine-node__ image yet, the build will automatically download it. This may take extra time but after it has been downloaded, future builds will be much faster.

# Running Containers
Once the image is built, it is just an image. It is not yet an instance of a container, for that it needs to be __run__. The container is created and initialized when it is first __run__.

Then run a container with this new image, this will create a new container.
```
docker run -d -p 3000:3000 <new image title>
```
The __-p 3000:3000__ will publish the service at port 3000 inside the container on the host OS. 

The __-d__ sets the container to run detached so it will fork to the background.


# Monitoring
At this point, the docker service is running and should respond to web requests. 

Open a browser to the IP address of the Linux host OS at port 3000

```
http://<linux>:3000
```

The node process can be seen running in the host OS.

From your host Linux system, run these commands.

```bash
ps -ef | grep node
netstat -naptu |grep 3000.*LISTEN
```

### List the status of running containers
```
docker ps 
docker container ls
```

### List the status of all containers.
```
docker ps -a
docker container ls -a
```


### Status
Get a top like status of the running containers.
```
docker stats
```

To just get a one time report of stats.
```
docker stats --no-stream
```


# Stopping Containers
Containers must be stopped before they can be deleted.

```
docker stop <container ID>
```

# Re-Starting Containers
Once a container has been ran, it has been initialized and can be restarted.

```
docker start <container ID>
```


# Deleting Containers
List the containers, use -a all to see the ones that are not running.
```
docker container ls -a
```

Containers must be deleted before the associated image can be deleted.

```
docker rm <container ID>
```



# Deleting Images
First, list the images.
```
docker image ls
```

Then use the image name or ID to delete it.
```
docker rmi <image name or ID>
```





