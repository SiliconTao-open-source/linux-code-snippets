#!/usr/bin/perl

# =============================================================================
#
#    Copyright (C) 2019 Silicon Tao Technology Systems Inc.
#    E-mail:  support@SiliconTao.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#=============================================================================

# New version 46 to solve these porblems
# 1) first row could be headers, if it does not start with digits, assume header
# 2) Multiple column data seems to get blended into identical values.
# 3) Need to change the delimiter so it can pipe directly to plotter
# 4) last line of output cannot ever have a lower time value then previous lines.


use DateTime;
use DateTime::Format::Strptime qw( );
my $Delimiter = ",";
my $TimeStepMinutes = 10;
my @AllFields = ("min", "max", "mean", "sum", "count", "std", "del", "cnz");
my $Format = "max";
my $Debug = 0;

sub ShowHelp {
	print" Gbt.pl will take columns of numbers and calculate the min/mean/max within a range of time from the first column.
The range of time is a block of minutes, the default is a 10 minute block. Within that block of time all numbers from each column after the first will be calculated for min/mean/max.

-s <minutes> : Set the time step size. Default is 10. To step by hours use multipes of 60 minutes.
-h : Show help screen and exit
-d : Enable debug mode for verbose messages of confusion.
-D <?> : Set the delimiter that is printed between cells (does not change format delimiter)
-f <format> : Select the output format fields seperated by a colon.
	Fields:
		max		: Maximum value in range 
		sum 	: sumation of all values in range 
		min		: Minimum values in range 
		del 	: Delta, difference from last value
		mean	: Average values in range (not working yet, just planning)
		std 	: Standard deviation (not working yet, just planning)
		count	: Count of samples
		cnz		: Count of non-zero values

	example 
		-f min:mean:max
\n";
	exit;
}

sub CalculateDel {
	my $Fieldname = shift @_;
	my $HashRef = shift @_;
	my $NextValue = shift @_;
	my $DeltaLastValue = 0;

	if($Debug == 1) { print "CalculateDel: Fieldname=$Fieldname\tWorkingValue='".$HashRef->{"last"}."'\tNextValue='$NextValue'\n"; }
	if(! defined $HashRef->{"last"}) {
		$HashRef->{"last"} = $NextValue;
	} 
	
	if($NextValue > $HashRef->{"last"}) {
		$HashRef->{"del"} = $NextValue - $HashRef->{"last"};
	}
	if($NextValue < $HashRef->{"last"}) {
		$HashRef->{"del"} = $HashRef->{"deltalast"} - $NextValue;
	}
	$HashRef->{"last"} = $NextValue;
	if($Debug == 1) { print "WorkingValue='".$HashRef->{"last"}."'\tNextValue='$NextValue'\n"; }
	return($HashRef->{"del"});
}

sub CalculateMax {
	my $Fieldname = shift @_;
	my $HashRef = shift @_;
	my $NextValue = shift @_;

	if($Debug == 1) { print "CalculateMax: Fieldname=$Fieldname\tWorkingValue='".$HashRef->{"max"}."'\tNextValue='$NextValue'\n"; }
	if(! defined $HashRef->{"max"}) {
		$HashRef->{"max"} = $NextValue;
	}
	if($NextValue > $HashRef->{"max"}) {
		if($Debug == 1) { print "replace workingValue='".$HashRef->{"max"}."'\tNextValue='$NextValue'\n"; }
		$HashRef->{"max"} = $NextValue;
	}
	return($HashRef->{"max"});
}

sub CalculateSum {
	my $Fieldname = shift @_;
	my $HashRef = shift @_;
	my $NextValue = shift @_;
	if($Debug == 1) { print "CalculateSum: Fieldname=$Fieldname\tWorkingValue=".$HashRef->{"sum"}."\tNextValue=$NextValue\n"; }
	if(! defined $HashRef->{"sum"}) {
		$HashRef->{"sum"} = $NextValue;
	} else {
		$HashRef->{"sum"} += $NextValue;
	}
	return($HashRef->{"sum"});
}

sub CalculateMin {
	my $Fieldname = shift @_;
	my $HashRef = shift @_;
	my $NextValue = shift @_;
	if($Debug == 1) { print "CalculateMin: Fieldname=$Fieldname\tWorkingValue=".$HashRef->{"min"}."\tNextValue=$NextValue\n"; }
	if(! defined $HashRef->{"min"}) {
		if($Debug == 1) { print "Set initial value\n"; }
		$HashRef->{"min"} = $NextValue;
	}
	if($HashRef->{"min"} > $NextValue) {
		$HashRef->{"min"} = $NextValue;
	}
	if($Debug == 1) { print "HashRef->min = ".$HashRef->{"min"}."\n"; }
	return($HashRef->{"min"});
}

sub CalculateStd {
	printf "line %d is not working yet.\n", __LINE__; 
}

sub Calculate {
	my $NextValue = shift @_;
	my $HashRef = shift @_;
	if($Debug == 1) { print "Calculate: Format=$Format\tHashRef{count}=".$HashRef->{"count"}."\tNextValue=$NextValue\n"; }
	# @CalcHash{split(/:/,$Format)} = split(/:/,$HashRef);
	if($Debug == 1) { 
		foreach $w (keys %$HashRef) {
			print "HashRef{$w}='".$HashRef->{$w}."'\n";
		}
	}

	$HashRef->{"sum"} += $NextValue;

	my @Fields = split(/:/,$Format);
	foreach (@Fields) {
		if($_ eq "max") 	{ CalculateMax($_, $HashRef, $NextValue); }
		if($_ eq "min") 	{ CalculateMin($_, $HashRef, $NextValue); }
		if($_ eq "std") 	{ CalculateStd($_, $HashRef, $NextValue); }
		if($_ eq "del") 	{ CalculateDel($_, $HashRef, $NextValue); }		
		if($_ eq "cnz") 	{ if($NextValue != 0) { $HashRef->{"cnz"}++; } }
	}
	$HashRef->{"count"}++;
	$HashRef->{"mean"} = $HashRef->{"sum"} / $HashRef->{"count"};

	if($Debug == 1) { print "Format=$Format\n"; }
	my $BuildString = "";
	foreach (split(/:/,$Format)) {
		if($BuildString ne "") {
			$BuildString .= ":";
		}
		$BuildString .= $HashRef->{$_};
		if($Debug == 1) { print "BuildString=$BuildString\n"; }
	}
	if($Debug == 1) { print "Calculate returns '$BuildString'\n"; }
	return($BuildString);
}

sub Preset {
	my $NextValue = shift @_;
	my $HashRef = shift @_;

	foreach (@AllFields) {
		$HashRef->{$_} = $NextValue;
	}
	$HashRef->{"count"} = 1;
	$HashRef->{"mean"} = $NextValue / 1;
	$HashRef->{"cnz"} = 0;
	if($NextValue != 0) { $HashRef->{"cnz"} = 1; }

	my $BuildString = "";
	if($Debug == 1) { print "Format=$Format\n"; }
	foreach (split(/:/,$Format)) {
		if($BuildString ne "") {
			$BuildString .= ":";
		}
		$BuildString .= $HashRef->{$_};
		if($Debug == 1) { print "BuildString=$BuildString\n"; }
	}
	if($Debug == 1) { print "Preset returns '$BuildString'\n"; }
	return($BuildString);
}

while ($#ARGV > -1) {
		my $Arg = shift @ARGV;
		if ($Arg eq "-s") { $TimeStepMinutes = shift @ARGV; }
		if ($Arg eq "-h") { ShowHelp(); }
		if ($Arg eq "-d") { $Debug = 1;}
		if ($Arg eq "-D") { $Delimiter = shift @ARGV; }
		if ($Arg eq "-f") { $Format = shift @ARGV; }
}
my %Rows;
my @Header;
my @Sequence;
my $EndTime = DateTime->new(year=>0001)->truncate(to => 'day');
my $StartTime = $EndTime->clone;

# This forces the minutes to start at zero
my $ForceMinuteRule = 1; 
my $LineCount = 0;
#my @DataSets;

my $ETT;
while (<>) {
	$LineCount++;
	#my %DataSet;
	if($Debug == 1) { print "\n---------------------------------------------------\n$_\n"; }
	@data = split(/\s+/);
	if ( $LineCount eq 1 ) {
		if (@data[0] =~ /^\D/) {
			@Header = @data;
			next;
		}
	}
	my $Nd = DateTime::Format::Strptime->new( pattern => '%H:%M:%S' )->parse_datetime( @data[0] );
	$Nd->set_second(0);
	if($Debug == 1) {
		print "Nd datetime = ".$Nd->datetime();
		if(defined($EndTime)) {
			print "\t EndTime datetime = ".$EndTime->datetime();
		}
		print "\n";
	}
	if ( DateTime->compare($Nd, $EndTime) == 1) {
	 	$StartTime = $EndTime->clone();
		$EndTime = $StartTime->clone->add(minutes => $TimeStepMinutes);		
		if($Debug == 1) { print "StartTime datetime = ".$StartTime->datetime()."\t EndTime datetime = ".$EndTime->datetime()."\n"; }
		if ( $Nd > $EndTime) {
			$StartTime = DateTime::Format::Strptime->new( pattern => '%H:%M:%S' )->parse_datetime( @data[0] );
			$StartTime->set_second(0);
			while($StartTime->minute() % $TimeStepMinutes) {
				if($Debug == 1) { print "Step back time = ".$StartTime->datetime()."\n"; }	
				$StartTime->subtract(minutes => 1);
			}
			$EndTime = $StartTime->clone->add(minutes => $TimeStepMinutes);
			if($Debug == 1) { print "New EndTime on second try = ".$EndTime->datetime()."\n"; }
		}  else {
			if($Debug == 1) { print "New EndTime on first try = ".$EndTime->datetime()."\n"; }
		}
		shift @data;

		$ETT = $EndTime->strftime("%H:%M:%S");
		push(@Sequence, $ETT);
		
		my @Columns;

		$i = 0;
		foreach (@data) {	
			$Columns[$i] = {};
			if($Debug == 1) { print "Pack initial data into column[$i] @ ".\%{$Columns[$i]}."\n"; }
			$Columns[$i]{'result'} = Preset($_, \%{$Columns[$i]});			
			$i++;
		}
		if($Debug == 1) { print "push Columns @ @Columns, ETT = $ETT\n"; }
		$Rows{$ETT} = \@Columns;
	} else {
		my $ColRef = $Rows{$ETT};
		if($Debug == 1) { print "use ColRef @ @{$ColRef}, ETT = $ETT\n"; }
		shift @data;
		$i = 0;
		foreach (@data) {			
			if($Debug == 1) { print "Pack updated data into column[$i] @ ".@{$ColRef}[$i]."\n"; }	
			${${$ColRef}[$i]}{'result'} = Calculate($_, @{$ColRef}[$i]);
			$i++;
		}
	}
}


if( scalar @Header ) {
	printf $Header[0];
	for my $i ( 1 .. $#Header) {
		print $Delimiter,$Header[$i];
	}
	printf "\n";
}

foreach (@Sequence) {
	print $_;
	if($Debug == 1) { print "seqence item $_\n"; }
	my $ColRef = $Rows{$_};
	if($Debug == 1) { printf "\nColumn array @ @{$ColRef}, count = ".scalar @{$ColRef}."\n"; }
	foreach ( @{$ColRef} ) {
		print $Delimiter.${$_}{"result"};
	}
	print "\n";
}
