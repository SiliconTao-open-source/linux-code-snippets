# What is Maven
Maven is a binary repository supported by the Apache foundation and is primarilly used to destribute Java libraries.

```
mvn -B archetype:generate -DartifactId=test -DgroupId=dev.mycode.test
```
[<img src="tree.png" alt="Screenshot" width="200px">](tree.png)

```
cd test
cat src/main/java/dev/mycode/test/App.java
```

Maven generated a simple **Hello World** program.
```java
package dev.mycode.test;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
```


Compile the program into a JAR.
```
mvn package
```

There should now be a file **target/test-1.0-SNAPSHOT.jar**, let's run it.
```
java -cp target/test-1.0-SNAPSHOT.jar dev.mycode.test.App
Hello World!
```


# Helpful links
https://www.oracle.com/webfolder/technetwork/tutorials/obe/java/Maven_SE/Maven.html

http://www.vineetmanohar.com/2009/11/3-ways-to-run-java-main-from-maven/

Projects often have differnet ways they are deployed. It is common for DEV, QAT and PROD to have special options that are unique to the environment. Maven supports this by using profiles in the pom.xml. The **-P** lets the developer select the profile to use.

https://maven.apache.org/guides/mini/guide-building-for-different-environments.html



