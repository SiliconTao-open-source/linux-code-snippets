# What is liquibase?
liquibase is a tool to manage the deployment of database changes. 

liquibase uses XML or SQL input files to populate changes to the DB. Changes are saved to the DB in tables DATABASECHANGELOG & DATABASECHANGELOGLOCK

This will create a small DB using SQLite then use liquibase to generate the XML for deployment. 
This is close to working but I kept getting Java errors trying to use **org.xerial.sqlite-jdbc** and that led me to playing with **MVN**.
```
echo "CREATE TABLE testing (id INT, name CHAR(60), mobile CHAR(30), company CHAR(50));" | sqlite3 test.db
echo "driver: org.sqlite.JDBC" > liquibase.properties
echo "url: jdbc:sqlite:test.db" >> liquibase.properties
liquibase --changeLogFile=test.xml generateChangeLog
```
