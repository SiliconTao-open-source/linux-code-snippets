#!/bin/bash

TMP=$(mktemp /dev/shm/JQ_XXXXXXXXXXXXXXX)
echo $TMP

echo Create new JSON data set.
sleep 1
jq -Rn '{ "firewall": { amiId: "ami-1234", base: "amazon-linux-2", built: "'$(date +%F_%T)'" }}' > $TMP
cat $TMP | jq
sleep 5

echo Append to a JSON data set.
sleep 1
cat $TMP | jq '.["webserver"].amiId="ami-5678"' > ${TMP}.swap && mv ${TMP}.swap $TMP
cat $TMP | jq '.["webserver"].base="centos-7"' > ${TMP}.swap && mv ${TMP}.swap $TMP
cat $TMP | jq
sleep 5

echo Get all values for an entry by name
sleep 1
cat $TMP | jq '.["firewall"]'
sleep 5

echo Get the amiId by name
sleep 1
cat $TMP | jq '.["firewall"].amiId'
sleep 5

echo Set the amiId by name
sleep 1
cat $TMP | jq '.["firewall"].amiId="AMI-9876"' # > ${TMP}.swap && mv swap $TMP
sleep 5

echo Select a data set by exact match
sleep 1
cat $TMP | jq 'with_entries(select(.value.base=="centos-7"))'
sleep 5

echo Select a data set by partial match
sleep 1
cat $TMP | jq 'with_entries(select(.value.base | startswith("amazon")))'
sleep 5

rm -f $TMP

