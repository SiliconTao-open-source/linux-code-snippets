#!/usr/bin/ruby

require 'date'

nextRow = 0
lastDate = ""
lastRev = ""
newDate = ""
newRev = ""
docs = Array.new

$stdin.each_line do |l|
 if l.include?("-"*72) 
  nextRow = 0
 else
   if nextRow > 0
    if l.chomp.length == 0
     nextRow += 1
    else
     docs.push(l)
    end
   else
    $Sl = l.split
    newRev = $Sl[0]
    newDate = $Sl[4]
    nextRow += 1
    if lastDate != ""
     if lastDate != newDate and docs.length > 0
      printf "* %s Revision %s\n", Date.parse(lastDate).strftime("%a %b %d %Y"), lastRev
      docs.each { |dl|
       puts "- #{dl}"
      }
      puts
      docs.clear
     end
    end
    lastDate = newDate
    lastRev = newRev
   end
 end
end

if docs.length > 0
 printf "* %s Revision %s\n", Date.parse(lastDate).strftime("%a %b %d %Y"), lastRev
 docs.each { |dl|
  puts "- #{dl}"
 }
end

