iptables -v --replace INPUT $(iptables -nvL INPUT --line-numbers | grep ^[1-9] | grep "4.3.2.6" | awk '{print $1}') -s 6.2.3.4 -d 4.3.2.6 -j ACCEPT
ACCEPT  all opt -- in * out *  6.2.3.4  -> 4.3.2.6  
iptables -nvL INPUT
Chain INPUT (policy ACCEPT 238 packets, 69612 bytes)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 DROP       all  --  *      *       1.2.3.4              4.3.2.1             
    0     0 ACCEPT     all  --  *      *       6.2.3.4              4.3.2.6             
    0     0 DROP       all  --  *      *       6.9.3.4              4.3.9.6             
iptables -v --replace INPUT $(iptables -nvL INPUT --line-numbers | grep ^[1-9] | grep "4.3.2.6" | awk '{print $1}') -s 6.2.3.4 -d 4.3.2.6 -j DROP
DROP  all opt -- in * out *  6.2.3.4  -> 4.3.2.6  
iptables -nvL INPUT
Chain INPUT (policy ACCEPT 18 packets, 2545 bytes)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 DROP       all  --  *      *       1.2.3.4              4.3.2.1             
    0     0 DROP       all  --  *      *       6.2.3.4              4.3.2.6             
    0     0 DROP       all  --  *      *       6.9.3.4              4.3.9.6             
iptables -v --delete INPUT $(iptables -nvL INPUT --line-numbers | grep ^[1-9] | grep "4.3.2.6" | awk '{print $1}')
iptables -nvL INPUT
Chain INPUT (policy ACCEPT 12 packets, 1631 bytes)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 DROP       all  --  *      *       1.2.3.4              4.3.2.1             
    0     0 DROP       all  --  *      *       6.9.3.4              4.3.9.6  

